<?php

namespace ProPhp\Params\Readme\Examples;

use ProPhp\Params;

class CustomParams extends Params
{
    protected bool $validate = true;

    public function isSilent(bool $validate = null): bool|self
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected null|string $wildcard = null;

    public function wildcard(string $wildcard = null): string|self|null
    {
        return $this->getOrSet(get_defined_vars());
    }

    protected int $daysToKeepBackups = 10;

    public function daysToKeepBackups(int $daysToKeepBackups = null): int|self
    {
        return $this->getOrSet(get_defined_vars());
    }
}